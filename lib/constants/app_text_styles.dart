import 'package:flutter/material.dart';

class AppTextStyle {
  static const TextStyle titleText = TextStyle(
    fontSize: 38,
    color: Color.fromARGB(255, 175, 38, 38),
    fontWeight: FontWeight.bold,
    fontFamily: 'Caveat',
  );
  static const TextStyle previewText = TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.w800,
    fontFamily: 'Caveat',
  );

  static const TextStyle loginText = TextStyle(
    fontSize: 48,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontFamily: 'Caveat',
  );

  static const TextStyle userText = TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontFamily: 'Caveat',
  );

  static const TextStyle registerText = TextStyle(
    fontSize: 18,
    color: Color.fromARGB(255, 251, 55, 55),
    fontWeight: FontWeight.bold,
    fontFamily: 'Caveat',
  );
}
