import 'package:flutter/material.dart';

class AppColors {
  static const Color brandColor = Color.fromARGB(255, 161, 20, 20);
  static const Color whiteColor = Colors.white;
  static const Color darkColor = Color.fromARGB(255, 21, 20, 20);
}
