import 'package:delivery/constants/app_colors.dart';
import 'package:delivery/constants/app_text_styles.dart';
import 'package:delivery/pages/register_page.dart';
import 'package:flutter/material.dart';

import '../extract_widget/loginform.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Container(
              color: Color.fromARGB(255, 22, 22, 22),
              child: Container(
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(40),
                    ),
                    color: AppColors.darkColor,
                    image: DecorationImage(
                        image: AssetImage('assets/images/delivery.jpeg'),
                        fit: BoxFit.cover)),
              ),
            ),
          ),
          Expanded(
            child: Container(
              color: AppColors.darkColor,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 40),
                decoration: const BoxDecoration(
                  color: AppColors.darkColor,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(30),
                  ),
                ),
                child: Column(
                  children: [
                    Text(
                      'Login In',
                      style: AppTextStyle.loginText,
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    LoginForm(),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'New user? ',
                          style: AppTextStyle.userText,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    RegisterPage(),
                              ),
                            );
                          },
                          child: Text(
                            'Register now.',
                            style: AppTextStyle.registerText,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
