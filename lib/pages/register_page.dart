import 'package:delivery/constants/app_colors.dart';
import 'package:delivery/constants/app_text_styles.dart';
import 'package:delivery/extract_widget/custom_button.dart';
import 'package:delivery/extract_widget/custom_input.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController firstname = TextEditingController();
    TextEditingController lastname = TextEditingController();
    TextEditingController email = TextEditingController();
    TextEditingController password = TextEditingController();
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Container(
              color: Color.fromARGB(255, 137, 22, 14),
              child: Stack(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Container(
                      width: 180,
                      height: 180,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(90),
                        image: const DecorationImage(
                            image: AssetImage('assets/images/pizzburglogo.jpg'),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  const Positioned(
                    bottom: 30,
                    left: 130,
                    child: Text(
                      'Sign Up',
                      style: AppTextStyle.loginText,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              color: const Color.fromARGB(255, 18, 17, 17),
              child: Column(
                children: [
                  CustomInput(
                    controller: firstname,
                    text: 'Firstname',
                    color: AppColors.whiteColor,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  CustomInput(
                    controller: lastname,
                    text: 'Lastname',
                    color: AppColors.whiteColor,
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  CustomInput(
                    controller: email,
                    text: 'Email',
                    color: AppColors.whiteColor,
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  CustomInput(
                    controller: password,
                    text: 'Password',
                    color: AppColors.whiteColor,
                  ),
                  const SizedBox(
                    height: 22,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: CustomButton(
                      color: AppColors.whiteColor,
                      text: 'Cancel',
                      onpressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: CustomButton(
                      color: AppColors.brandColor,
                      text: 'Register',
                      onpressed: () {},
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
