import 'package:delivery/constants/app_colors.dart';
import 'package:delivery/constants/app_text_styles.dart';
import 'package:delivery/pages/login_page.dart';
import 'package:delivery/pages/register_page.dart';
import 'package:flutter/material.dart';

import '../extract_widget/custom_button.dart';

class GetStarted extends StatefulWidget {
  const GetStarted({super.key});

  @override
  State<GetStarted> createState() => _GetStartedState();
}

class _GetStartedState extends State<GetStarted> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Container(
              color: Color.fromARGB(255, 137, 22, 14),
              child: Center(
                child: Container(
                  width: 180,
                  height: 180,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(90),
                    image: DecorationImage(
                        image: AssetImage('assets/images/logo2.jpeg'),
                        fit: BoxFit.cover),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 40, vertical: 30),
              color: const Color.fromARGB(255, 18, 17, 17),
              child: Column(
                children: [
                  Text(
                    'Welcome to PizzBurg',
                    style: AppTextStyle.titleText,
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Text(
                    'Try our delicious pizza anywhere, anytime! they are always fresh and delicious',
                    style: AppTextStyle.previewText,
                    textAlign: TextAlign.center,
                    maxLines: 3,
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  CustomButton(
                    color: AppColors.brandColor,
                    text: 'Login',
                    onpressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => LoginPage(),
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  CustomButton(
                    onpressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => RegisterPage(),
                        ),
                      );
                    },
                    color: AppColors.whiteColor,
                    text: 'Sign Up',
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
