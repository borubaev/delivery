import 'package:flutter/material.dart';

class CustomInput extends StatelessWidget {
  const CustomInput({
    super.key,
    required this.text,
    required this.color,
    this.icon,
    required this.controller,
  });
  final String text;
  final Color color;
  final Icon? icon;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(left: 10),
        filled: true,
        fillColor: color,
        prefixIcon: icon,
        hintText: '$text',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
          borderSide: BorderSide.none,
        ),
      ),
    );
  }
}
