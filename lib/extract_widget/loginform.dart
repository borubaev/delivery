import 'package:flutter/material.dart';

import '../constants/app_colors.dart';
import 'custom_input.dart';

class LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextEditingController login = TextEditingController();
    TextEditingController password = TextEditingController();
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 30,
      ),
      child: Column(
        children: [
          Container(
            height: 40,
            child: CustomInput(
              controller: login,
              text: 'Username',
              color: AppColors.whiteColor,
              icon: Icon(Icons.person),
            ),
          ),
          SizedBox(height: 22),
          Container(
              height: 40,
              child: CustomInput(
                controller: password,
                text: 'Password',
                color: AppColors.whiteColor,
                icon: Icon(Icons.lock_outline),
              )),
          SizedBox(height: 24),
          ElevatedButton(
            onPressed: () {},
            style: ElevatedButton.styleFrom(
              primary: Colors.red[800],
            ),
            child: Text('Login'),
          ),
        ],
      ),
    );
  }
}
